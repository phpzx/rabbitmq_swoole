<?php
/**
 * Created by PhpStorm.
 * User: cleargo
 * Date: 2021/11/28
 * Time: 17:29
 */
//设计一个秒杀系统

$redis = new Redis();
$random = mt_rand(1,1000).'-'.gettimeofday(true).'-'.mt_rand(1,1000);

$tt1 = 4;
$lock = false;
while(!$lock) {
    $lock = $redis->set('lock',$random,array('nx','ex' => $tt1));
}

if($redis->get('goods.num') <= 0) {
    echo ('秒杀已经结束');
    if($redis->get('lock') == $random) {
         $redis->del('lock');
    }
    return false;
}
$redis->decr('goods.num');
echo ('秒杀成功');
if($redis->get('goods.num') == $random) {
    $redis->del('lock');
}
return true;

 /*
    核心总结：
    1.秒杀前一般都是知道库存的，先统计库存，然后写到redis,
 */

 /*
  * 整体思路
  * 例如我们要秒杀100件商品
  * 1.redis设置参数值100，设置一个秒杀成功空队列
  * 2.开始秒杀，秒杀成功的用户ID塞进队列
  * 3.读取用户队列，与商品ID绑定，生成订单
  * 
  * */

